<!-- DRAFT Instructions for PowerISA Version 3.0 B Book 1 -->
<!-- https://libre-soc.org/openpower/sv/bitmanip/ -->
<!-- https://libre-soc.org/openpower/sv/av_opcodes/ -->

# DRAFT Fixed Point Signed Max

X-Form

* maxs  RT,RA,RB (Rc=0)
* maxs. RT,RA,RB (Rc=1)

Pseudo-code:

    if   (RA) > (RB) then RT <- (RA)
    else                  RT <- (RB)

Special Registers Altered:

    CR0                     (if Rc=1)

# DRAFT Fixed Point Unsigned Max

X-Form

* maxu  RT,RA,RB (Rc=0)
* maxu. RT,RA,RB (Rc=1)

Pseudo-code:

    if   (RA) >u (RB) then RT <- (RA)
    else                   RT <- (RB)

Special Registers Altered:

    CR0                     (if Rc=1)

# DRAFT Fixed Point Signed Min

X-Form

* mins  RT,RA,RB (Rc=0)
* mins. RT,RA,RB (Rc=1)

Pseudo-code:

    if   (RA) < (RB) then RT <- (RA)
    else                  RT <- (RB)

Special Registers Altered:

    CR0                     (if Rc=1)

# DRAFT Fixed Point Unsigned Min

X-Form

* minu  RT,RA,RB (Rc=0)
* minu. RT,RA,RB (Rc=1)

Pseudo-code:

    if   (RA) <u (RB) then RT <- (RA)
    else                   RT <- (RB)

Special Registers Altered:

    CR0                     (if Rc=1)

# DRAFT Average Add

X-Form

* avgadd  RT,RA,RB (Rc=0)
* avgadd. RT,RA,RB (Rc=1)

Pseudo-code:

    a <- [0] * (XLEN+1)
    b <- [0] * (XLEN+1)
    a[1:XLEN] <- (RA)
    b[1:XLEN] <- (RB)
    r <- (a + b + 1)
    RT <- r[0:XLEN-1]

Special Registers Altered:

    CR0                     (if Rc=1)

# DRAFT Absolute Signed Difference

X-Form

* absds  RT,RA,RB (Rc=0)
* absds. RT,RA,RB (Rc=1)

Pseudo-code:

    if (RA) < (RB) then RT <- ¬(RA) + (RB) + 1
    else                RT <- ¬(RB) + (RA) + 1

Special Registers Altered:

    CR0                     (if Rc=1)

# DRAFT Absolute Unsigned Difference

X-Form

* absdu  RT,RA,RB (Rc=0)
* absdu. RT,RA,RB (Rc=1)

Pseudo-code:

    if (RA) <u (RB) then RT <- ¬(RA) + (RB) + 1
    else                RT <- ¬(RB) + (RA) + 1

Special Registers Altered:

    CR0                     (if Rc=1)

# DRAFT Absolute Accumulate Unsigned Difference

X-Form

* absdacu  RT,RA,RB (Rc=0)
* absdacu. RT,RA,RB (Rc=1)

Pseudo-code:

    if (RA) <u (RB) then r <- ¬(RA) + (RB) + 1
    else                 r <- ¬(RB) + (RA) + 1
    RT <- (RT) + r

Special Registers Altered:

    CR0                     (if Rc=1)

# DRAFT Absolute Accumulate Signed Difference

X-Form

* absdacs  RT,RA,RB (Rc=0)
* absdacs. RT,RA,RB (Rc=1)

Pseudo-code:

    if (RA) < (RB) then r <- ¬(RA) + (RB) + 1
    else                r <- ¬(RB) + (RA) + 1
    RT <- (RT) + r

Special Registers Altered:

    CR0                     (if Rc=1)

# Carry Propagate

X-Form

* cprop RT,RA,RB (Rc=0)
* cprop RT,RA,RB (Rc=1)

Pseudo-code:

    P <- (RA)
    G <- (RB)
    temp <- (P|G)+G
    RT <- temp^P 

Special Registers Altered:

    CR0                    (if Rc=1)

# DRAFT Bitmanip Masked
 
BM2-Form
 
* bmask  RT,RA,RB,bm

Pseudo-code:

    if _RB = 0 then mask <- [1] * XLEN
    else mask <- (RB)
    ra <- (RA) & mask
    a1 <- ra
    if bm[4] = 0 then a1 <- ¬ra
    mode2 <- bm[2:3]
    if mode2 = 0 then a2 <- (¬ra)+1
    if mode2 = 1 then a2 <- ra-1
    if mode2 = 2 then a2 <- ra+1
    if mode2 = 3 then a2 <- ¬(ra+1)
    a1 <- a1 & mask
    a2 <- a2 & mask
    # select operator
    mode3 <- bm[0:1]
    if mode3 = 0 then result <- a1 | a2
    if mode3 = 1 then result <- a1 & a2
    if mode3 = 2 then result <- a1 ^ a2
    if mode3 = 3 then result <- undefined([0]*XLEN)
    result <- result & mask
    # optionally restore masked-out bits
    if L = 1 then
        result <- result | (RA & ¬mask)
    RT <- result

Special Registers Altered:

    None

