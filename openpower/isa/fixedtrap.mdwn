<!-- This defines instructions described in PowerISA Version 3.0 B Book 1 -->

<!-- Section 3.3.11 Fixed-Point Trap Instructions pages 89 - 91 -->

<!-- The Trap instructions are provided to test for a specified set of conditions. -->
<!-- If any of the conditions tested by a Trap instruction are met, the system trap -->
<!-- handler is invoked. If none of the tested conditions are met, instruction -->
<!-- execution continues normally. -->

# Trap Word Immediate

D-Form

* twi TO,RA,SI

Pseudo-code:

    a <- EXTS((RA)[XLEN/2:XLEN-1])
    if (a < EXTS(SI)) & TO[0]  then TRAP
    if (a > EXTS(SI)) & TO[1]  then TRAP
    if (a = EXTS(SI)) & TO[2]  then TRAP
    if (a <u EXTS(SI)) & TO[3] then TRAP
    if (a >u EXTS(SI)) & TO[4] then TRAP

Special Registers Altered:

    None

# Trap Word

X-Form

* tw TO,RA,RB

Pseudo-code:

    a <- EXTS((RA)[XLEN/2:XLEN-1])
    b <- EXTS((RB)[XLEN/2:XLEN-1])
    if (a < b) & TO[0] then TRAP
    if (a > b) & TO[1] then TRAP
    if (a = b) & TO[2] then TRAP
    if (a <u b) & TO[3] then TRAP
    if (a >u b) & TO[4] then TRAP

Special Registers Altered:

    None

# Trap Doubleword Immediate

D-Form

* tdi TO,RA,SI

Pseudo-code:

    a <- (RA)
    b <- EXTS(SI)
    if (a < b) & TO[0] then TRAP
    if (a > b) & TO[1] then TRAP
    if (a = b) & TO[2] then TRAP
    if (a <u  b) & TO[3] then TRAP
    if (a >u  b) & TO[4]  then TRAP

Special Registers Altered:

    None

# Trap Doubleword

X-Form

* td TO,RA,RB

Pseudo-code:

    a <- (RA)
    b <- (RB)
    if (a < b) & TO[0] then TRAP
    if (a > b) & TO[1] then TRAP
    if (a = b) & TO[2] then TRAP
    if (a <u b) & TO[3] then TRAP
    if (a >u b) & TO[4]  then TRAP

Special Registers Altered:

    None

# Integer Select

A-Form

* isel RT,RA,RB,BC

Pseudo-code:

    if CR[BC+32]=1 then RT <- (RA|0)
    else                RT <- (RB)

Special Registers Altered:

    None

<!-- Checked March 2021 -->
