<!-- Draft Instructions here described in https://libre-soc.org/openpower/sv/bitmanip/ -->
<!-- These instructions are *not yet official* -->

# Ternary Bitwise Logic Immediate

TLI-Form

* ternlogi RT, RA, RB, TLI
* ternlogi. RT, RA, RB, TLI

Pseudo-code:

    result <- [0] * XLEN
    do i = 0 to XLEN - 1
      idx <- (RT)[i] || (RA)[i] || (RB)[i]
      result[i] <- TLI[7-idx]
    RT <- result

Special Registers Altered:

    CR0                    (if Rc=1)

# Generalized Bit-Reverse

X-Form

* grev RT, RA, RB (Rc=0)
* grev. RT, RA, RB (Rc=1)

Pseudo-code:

    result <- [0] * XLEN
    b <- EXTZ64(RB)
    do i = 0 to XLEN - 1
      idx <- b[64-log2(XLEN):63] ^ i
      result[i] <- (RA)[idx]
    RT <- result

Special Registers Altered:

    CR0                    (if Rc=1)

# Generalized Bit-Reverse Immediate

XB-Form

* grevi RT, RA, XBI (Rc=0)
* grevi. RT, RA, XBI (Rc=1)

Pseudo-code:

    result <- [0] * XLEN
    do i = 0 to XLEN - 1
      idx <- XBI[6-log2(XLEN):5] ^ i
      result[i] <- (RA)[idx]
    RT <- result

Special Registers Altered:

    CR0                    (if Rc=1)

# Generalized Bit-Reverse Word

X-Form

* grevw RT, RA, RB (Rc=0)
* grevw. RT, RA, RB (Rc=1)

Pseudo-code:

    result <- [0] * (XLEN / 2)
    a <- (RA)[XLEN/2:XLEN-1]
    b <- EXTZ64(RB)
    do i = 0 to XLEN / 2 - 1
      idx <- b[64-log2(XLEN/2):63] ^ i
      result[i] <- a[idx]
    RT <- ([0] * (XLEN / 2)) || result

Special Registers Altered:

    CR0                    (if Rc=1)

# Generalized Bit-Reverse Word Immediate

X-Form

* grevwi RT, RA, SH (Rc=0)
* grevwi. RT, RA, SH (Rc=1)

Pseudo-code:

    result <- [0] * (XLEN / 2)
    a <- (RA)[XLEN/2:XLEN-1]
    do i = 0 to XLEN / 2 - 1
      idx <- SH[5-log2(XLEN/2):4] ^ i
      result[i] <- a[idx]
    RT <- ([0] * (XLEN / 2)) || result

Special Registers Altered:

    CR0                    (if Rc=1)
